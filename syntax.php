<?php
/**
 * DokuWiki Plugin mtg (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Jamin Collins <jamin.collins@gmail.com>
 *
 * 2018/06/07 : Initial Release
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) {
    die();
}

class syntax_plugin_mtg extends DokuWiki_Syntax_Plugin
{
    /**
     * @return string Syntax mode type
     */
    public function getType()
    {
        return 'substition';
    }

    /**
     * @return string Paragraph type
     */
    public function getPType()
    {
        return 'normal';
    }

    /**
     * @return int Sort order - Low numbers go before high numbers
     */
    public function getSort()
    {
        return 25; // Internal link is 300, AFAIK
    }

    /**
     * Connect lookup pattern to lexer.
     *
     * @param string $mode Parser mode
     */
    public function connectTo($mode)
    {
        // TODO -- come up with syntax for matching cards???
        $this->Lexer->addSpecialPattern('{{mtg:.*?}}', $mode, 'plugin_mtg');
    }

    /**
     * Strip characters that scryfall and html don't like from card names.
     *
     * @param string    $card_name  MtG card name
     *
     * @return string   stripped card name
     */
    private function strip_card_name($card_name)
    {
        $card_name_stripped = str_replace("'", "", $card_name);
        return $card_name_stripped;
    }

    /**
     * Handle matches of the mtg syntax
     *
     * @param string       $match   The match of the syntax
     * @param int          $state   The state of the handler
     * @param int          $pos     The position in the document
     * @param Doku_Handler $handler The handler
     *
     * @return array Data for the renderer
     */
    public function handle($match, $state, $pos, Doku_Handler $handler)
    {
        // get card name from the match
        preg_match("~{{mtg:(.*)}}~", $match, $matches);
        list($card_name, $edition) = explode("|", $matches[1]);

        // get image uri from Scryfall's API
       $url = "https://api.scryfall.com/cards/named?exact="
               . urlencode($this->strip_card_name($card_name));
        if ($edition != null) {
            $url .= "&set=" . $edition;
        }
        $resp = file_get_contents($url);
        $obj = json_decode($resp, true);

        $image_uri = $obj['image_uris']['normal'];
        $data = array($card_name, $pos, $image_uri);

        return $data;
    }

    /**
     * Render xhtml output or metadata
     *
     * @param string        $mode     Renderer mode (supported modes: xhtml)
     * @param Doku_Renderer $renderer The renderer
     * @param array         $data     The data from the handler() function
     *
     * @return bool If rendering was successful.
     */
    public function render($mode, Doku_Renderer $renderer, $data)
    {
        if ($mode !== 'xhtml') {
            return false;
        }

        $card_name = $data[0];
        $pos = $data[1];
        $image_uri = $data[2];

        // create a unique id based on the card name and the position within the document
        $id = "mtg-" . str_replace(" ", "_", strtolower($this->strip_card_name($card_name))) . "-$pos";
        $gatherer_url = "http://gatherer.wizards.com/Pages/Card/Details.aspx?name=" . urlencode($card_name);
        $transparent_pixel = "lib/plugins/mtg/images/transparent.png";

        $output = "<a href='$gatherer_url' class='mtg urlextern' ";
        $output .= "onmouseover=\"document.getElementById('$id').src='$image_uri'; document.getElementById('$id').width='300';\" ";
        $output .= "onmouseout=\"document.getElementById('$id').src='$transparent_pixel'; document.getElementById('$id').width=0\"";
        $output .= ">$card_name<img class='mtg' src='$transparent_pixel' id='$id' /></a>";

        $renderer->doc .= $output;

        return true;
    }
}

